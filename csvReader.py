import pandas as pd
import numpy as np

# CURRENTLY READS FROM COPIED FILE TO PREVENT BREAKING ON FIRST INDEX
dataFrame = pd.read_csv("PythonExportCopy.csv")

# print(data.head())
dataFrame = dataFrame.loc[:, 'Item':]
print(dataFrame)
filteredFrame = dataFrame
dataFrame["selProfit"] = np.nan


def isInt(value):
    try:
        int(value)
        return True
    except ValueError:
        return False


for index, row in dataFrame.iterrows():
    if isInt(row['Pricebought at']) and isInt(row['GE price']):
        price = int(row['Pricebought at']) - int(row['GE price'])
        if price > 100:
            dataFrame.set_value(index, 'selProfit', price)

    else:
        dataFrame.drop([index])
        # print("dropped index %d" % index)

droppedEmptyFrame = dataFrame.dropna()
print(droppedEmptyFrame)
droppedEmptyFrame['multiplier'] = 1

for index, row in droppedEmptyFrame.iterrows():
    droppedEmptyFrame.set_value(index, 'multiplier', int(row['Pricebought at']) / int(row['GE price']))

print(droppedEmptyFrame)

droppedEmptyFrame = droppedEmptyFrame.sort_values(by=['selProfit'],ascending=True)
print(droppedEmptyFrame.to_string())
#     dataFrame.drop([index])
# dataFrame['combined'] = ((dataFrame['GE price']) - (dataFrame['Pricebought at']))
# print(dataFrame.to_string())
